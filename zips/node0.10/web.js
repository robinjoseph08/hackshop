var express = require('express')
  , app = express();

app.set('view engine', 'jade');
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));
app.use(app.router);

// home route
app.get('/', function (req, res) {
  console.log('GET ' + req.url);
  res.render('index');
});

var port = process.env.PORT || 3000;

app.listen(port, function() {
  console.log('Listening on port ' + port);
});