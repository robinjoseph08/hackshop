var express = require('express')
  , engine = require('ejs-locals')
  , app = express()
  , http = require('http')
  , mongo = require('mongojs')
  , spawn = require('child_process').spawn;

var port = process.env.PORT || 3000;

// configure the database URL
var database_url = 'hackshop';
// establish the mongo tables
var collections = ['user', 'hacklet'];
// connect to the mongo DB
var db = mongo.connect(database_url, collections);

app.set('view engine', 'ejs');
app.engine('ejs', engine);
app.set('views', __dirname + '/views');
app.use(express.bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(express.cookieParser(process.env.SESSION_KEY || 'SECRETKEY'));
app.use(express.session({
  secret: process.env.SESSION_KEY || 'SECRETKEY'
}));
app.use(app.router);

// home get handler
app.get('/', function(req, res) {
  console.log('get home');
  res.render('index', { user: req.session.username });
});

// signin get handler
app.get('/signin', function(req, res) {
  console.log('get signin');
  res.render('signin', { user: req.session.username });
});

// signin post handler
app.post('/signin', function(req, res) {
  console.log('post signin');

  // stores the user name and password into variables
  var user = req.body.user.login;
  var pass = req.body.user.pass;
  console.log(user);
  console.log(pass);

  db.user.find({username:user}, function(err, user) {
    if(!err && user.length) {
      console.log(user);
      req.session.username = user[0].username;
      console.log(req.session.username);
      res.redirect('/');
    } else {
      console.log('login error');
      res.render('signin', { user: req.session.username });
    }
  });
});

// sign up get handler
app.get('/signup', function(req, res) {
  console.log('post signup');
  res.render('signup', { user: req.session.username });
});

// sign up post handler
app.post('/signup', function(req, res) {
  console.log('post signup');
  console.log(req.body);
  var user = {
    email: req.body.user.email,
    password: req.body.user.password,
    username: req.body.user.username
  };
  console.log(user);

  db.user.find({username:req.body.user.username}, function(err, users) {
    if(!err && users.length) {
      req.session.username = req.body.user.username;
      res.redirect('/hacklets');
    } else if(!err) {
      db.user.save(user, function(err, saved) {
        if(err || !saved)
          console.log("User not saved");
        else
          console.log("User saved");
        console.log(saved);
        req.session.username = req.body.user.username;
        res.redirect('/hacklets');
      });
    } else {
      console.log('login error');
      res.render('signin', { user: req.session.username });
    }
  });
});

// hacklets get handler
app.get('/hacklets', function(req, res) {
  console.log('get hacklets');
  if(req.session.username) {
    db.hacklet.find({username:req.session.username}, function(err, hacklets) {
      if(!err) {
        console.log(hacklets);
        res.render('hacklets', { user: req.session.username, hacklets: hacklets });
      } else {
        console.log('find error');
        res.render('signin', { user: req.session.username });
      }
    });
  } else {
    res.redirect('/signin');
  }
});

// hacklets get handler
app.get('/hacklets/new', function(req, res) {
  console.log('get hacklets new');
  if(req.session.username) {
    res.render('hacklets/new', { user: req.session.username });
  } else {
    res.redirect('/signin');
  }
});

// hacklets get handler
app.post('/hacklets/new', function(req, res) {
  name = 'BreezyMountains';
  // image_id
  // https://api.digitalocean.com/images/?client_id=b693d3f4cfceb4f433fa38ccbf2d1646&api_key=99a3a352aa2500cd50a80bb2f3a32824
  image_id = '961965'
  // region_id
  // 4 = NYC 2
  is_node = true
  region_id = '4'
  console.log('https://api.digitalocean.com' + '/droplets/new?client_id=b693d3f4cfceb4f433fa38ccbf2d1646&api_key=99a3a352aa2500cd50a80bb2f3a32824&name=' + name  + '&size_id=66&image_id=' + image_id + '&region_id=' + region_id + "&ssh_key_ids=''")
  var options = {host: 'api.digitalocean.com', path: '/droplets/new?client_id=b693d3f4cfceb4f433fa38ccbf2d1646&api_key=99a3a352aa2500cd50a80bb2f3a32824&name=' + name  + '&size_id=66&image_id=' + image_id + '&region_id=' + region_id + "&ssh_key_ids=53043"};
  //var options = {host: 'api.digitalocean.com', path: '/ssh_keys/?client_id=b693d3f4cfceb4f433fa38ccbf2d1646&api_key=99a3a352aa2500cd50a80bb2f3a32824'};

  http.get(options, function(resp){
    resp.on('data', function(chunk){
      console.log()
      console.log('new droplet created')
      console.log(JSON.parse(chunk));
      droplet_id = JSON.parse(chunk).droplet.id
      // droplet = JSON.parse(droplet)
      console.log('chunk =  ' + chunk)
      console.log('droplet =  ' + droplet_id)

      options = {host: 'api.digitalocean.com', path: '/droplets/' + droplet_id + '?client_id=b693d3f4cfceb4f433fa38ccbf2d1646&api_key=99a3a352aa2500cd50a80bb2f3a32824'};
      setTimeout(function() {
        http.get(options, function(resp){
          resp.on('data', function(droplet_chunk){
            console.log('droplet_chunk_ip_address =  ' + JSON.parse(droplet_chunk).droplet.ip_address)
            //get the ip_address for "hacklet"
            ip_address = JSON.parse(droplet_chunk).droplet.ip_address


            //ssh into new droplet and install git and haproxy
            console.log('SSH TIME')
            ssh = spawn('ssh', ['-tt', 'root@' + ip_address]);
            ssh.stdout.on('data', function (data) {
              console.log('stdout: ' + data);
              ssh.stdin.end();
            });

            ssh.stderr.on('data', function (data) {
              console.log('stderr: ' + data);
            });

            ssh.on('close', function (code) {
              console.log('child process exited with code ' + code);
            });
            command_1 = 'yes | apt-get update; yes | apt-get install vim git g++ curl libssl-dev apache2-utils build-essential chrpath git-core libfontconfig1-dev libpq-dev libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev build-essential imagemagick libmagickcore-dev; useradd -m git; ';
            command_2 = 'git clone git://github.com/ry/node.git; cd node; git checkout v0.10.12; sudo ./configure; sudo make; sudo make install; cd ..; mkdir source; mkdir source.git; cd source.git; git init --bare; ';
            command_3 = 'echo \'#!/bin/sh\' >> hooks/post-receive; echo \'GIT_WORK_TREE=/home/git/source git checkout -f\' >> hooks/post-receive; echo \'cd /home/git/source\' >> hooks/post-receive; echo \'npm install .\' >> hooks/post-receive; echo \'pkill -f "$(cat Procfile | sed s/"web: "/""/)"\' >> hooks/post-receive; echo \'NODE_ENV="production" PORT=3000 nohup $(cat Procfile | sed s/"web: "/""/) > prog.log 2>&1 &\' >> hooks/post-receive; chmod +x hooks/post-receive; iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3000; ';
            final_command = command_1 + command_2 + command_3
            ssh.stdin.write(final_command + '\n', 'utf-8', function() {
              console.log('flushed');
            });



            // options = {host: 'api.digitalocean.com', path: '/droplets/' + droplet_id + '/destroy/?client_id=b693d3f4cfceb4f433fa38ccbf2d1646&api_key=99a3a352aa2500cd50a80bb2f3a32824'};
            // setTimeout(function() {
            //   http.get(options);
            // },100000);
          });
        });
      },60000);
    });
  }).on("error", function(e){console.log("Got error: " + e.message);});
  res.redirect('/loadbalancer/edit');
});

// hacklets get handler
app.get('/loadbalancer/edit', function(req, res) {
  console.log('get hacklets new');
  if(req.session.username) {
    res.render('loadbalancer', { user: req.session.username });
  } else {
    res.redirect('/signin');
  }
});

// hacklets get handler
app.post('/loadbalancer/edit', function(req, res) {
  console.log('get hacklets new');
  res.redirect('/hacklets#t');
});

// purchase handler
app.get('/purchase', function(req, res) {
  console.log('get purchase');

  res.writeHead(200);
  res.end();
});

// assets route
app.get('/(*).(css|js)', function (req, res) {
  //console.log('GET ' + req.url);
  res.sendfile(__dirname + '/' + req.params[1] + '/' + req.params[0] + '.' + req.params[1]);
});
// images route
app.get('/images/(*).(jpg|jpeg|png)', function (req, res) {
  console.log('GET ' + req.url);
  res.sendfile(__dirname + '/images/' + req.params[0] + '.' + req.params[1]);
});
// fonts route
app.get('/fonts/(*).(eot|svg|ttf|woff)', function (req, res) {
  console.log('GET ' + req.url);
  res.sendfile(__dirname + '/fonts/' + req.params[0] + '.' + req.params[1]);
});
// zips route
app.get('/zips/(*).zip', function (req, res) {
  console.log('GET ' + req.url);
  res.sendfile(__dirname + '/zips/' + req.params[0] + '.zip');
});

// 404 get handler
app.get('/*', function(req, res) {
  res.render('404', { user: req.session.username });
});

// listens on port "3000"
app.listen(port, function() {
  console.log('Listening on port ' + port);
});